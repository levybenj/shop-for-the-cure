import pytest

from sftc import api


def test_api_simple():
    asin = "B01MSU0YKK"
    jsonResponse = api.asin_api_call(asin)
    assert jsonResponse['brand'] == 'Tide'
