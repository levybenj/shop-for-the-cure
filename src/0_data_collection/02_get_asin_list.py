from sftc import config

import pandas as pd


def load_data():
    return pd.read_csv(config.data / 'raw' / 'sa_purchases_raw.csv')


def process_asin(asin):
    try:
        return asin.split(',')[0].strip('"[]"')
    except AttributeError:
        return 'none'


def get_asins(df):
    asins = df['base_info:extra.matched_asins'].apply(process_asin)
    return asins


def drop_dups_and_none(asins):
    return asins[asins != 'none'].unique()


def write_to_file(asins, fname):
    with (config.data / 'processed' / fname).open('w') as fout:
        for a in asins:
            fout.write(a + '\n')


def main():
    data = load_data()
    asins = get_asins(data)
    asins = drop_dups_and_none(asins)
    write_to_file(asins, 'asins.txt')


if __name__ == '__main__':
    main()
