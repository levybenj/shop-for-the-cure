import requests
from lxml import etree

from sftc import config, utils


pg_brand_url = 'https://www.pg.ca/en-ca/brands-and-products/'
ua_string = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.77 Safari/537.36'
css_selector = 'h4.brand__headline'
parser = etree.HTMLParser()


def get_page(url: str):
    headers = {
        'User-Agent': ua_string
    }
    return requests.get(url, headers=headers).text


def parse_html(html: str):

    parsed = etree.fromstring(html, parser)
    tags = parsed.cssselect(css_selector)
    return [t.text for t in tags]


def main():
    html = get_page(pg_brand_url)
    brands = parse_html(html)
    with (config.data / 'raw' / 'pg_brands.txt').open('w') as fout:
        for brand in brands:
            fout.write(brand)
            fout.write('\n')


if __name__ == '__main__':
    main()
