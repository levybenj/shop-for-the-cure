import re

import pandas as pd
import numpy as np

from sftc import config, utils


def load_data():
    return pd.read_csv(config.final / 'purchase_features.csv', index_col=0)


def drop_empty_rows(df):
    return df[~pd.isna(df['date'])]


def generate_features(df):
    def cashback(row):
        try:
            return utils.convert_comma_to_float(row['price']) * int(row['rate']) / 100
        except ValueError:
            return 0

    def actual_spent(row):
        try:
            # print(type(row['price']))
            return utils.convert_comma_to_float(row['price']) * (1 - int(row['rate']) / 100)
        except ValueError:
            return utils.convert_comma_to_float(row['price'])

    def is_bonus(x, rate):
        try:
            return int(int(x['rate']) == rate)
        except ValueError:
            return 0

    def is_advertiser(x, adv):
        try:
            return int(x['advertiser'] == adv)
        except ValueError:
            return 0

    df = df.assign(
            actual_spent=df.apply(actual_spent, axis=1),
            cashback=df.apply(cashback, axis=1),
            bonus50=df.apply(is_bonus, args=(50,), axis=1),
            bonus18=df.apply(is_bonus, args=(18,), axis=1),
            bonus13=df.apply(is_bonus, args=(13,), axis=1),
            amazon=df.apply(is_advertiser, args=('Amazon.ca',), axis=1),
            walmart=df.apply(is_advertiser, args=('Walmart Canada',), axis=1),
            well=df.apply(is_advertiser, args=('Well.ca',), axis=1),
            sephora=df.apply(is_advertiser, args=('Sephora',), axis=1),
            shoppers=df.apply(is_advertiser, args=('Shoppers Drug Mart',), axis=1)
        )
    return df


def rename_cols(df):
    return df.rename(columns={
            'purchase_type': 'pg_brand'
        })


def main():
    df_purchases = load_data()
    df_purchases = drop_empty_rows(df_purchases)
    df_purchases = generate_features(df_purchases)
    df_purchases = rename_cols(df_purchases)
    df_purchases.to_csv(config.final / 'purchase_features.csv')


if __name__ == '__main__':
    main()
