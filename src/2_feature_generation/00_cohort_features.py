import re

import pandas as pd

from sftc import config, utils


def load_data():
    df_members = utils.load_raw_members_data()
    purchases = pd.read_csv(config.data / 'processed' / 'sa_labelled_purchases.csv', index_col=0)
    return df_members, purchases


def rename_cols(df):
    df = df[[c for c in df.columns if not re.search(r'gt\d$', c)]]
    df.columns = [c.split(":")[-1] for c in df.columns]
    return df


def label_crm_members(df_members):
    df_members['pg_crm'] = df_members['landing_url'].apply(utils.is_crm_member)
    return df_members


def subset_cols(df):
    cols_to_keep = [
        'key',
        'id',
        'created_at',
        'days_since_created_at',
        'is_canadian',
        'gender',
        'yob',
        'num_amazon_valid_trips',
        'days_since_last_purchase',
        'total_spent',
        'pg_crm'
    ]
    df = df[cols_to_keep]
    return df


def format_vars(df):
    for var in ['id', 'yob', 'days_since_last_purchase']:
        df[var] = df[var].apply(utils.convert_comma_to_int)
    return df


def generate_member_features(df_members, df_purchases):
    """
    Features to generate
        1. crm
        2. spend_type
        3. highest_bonus
        4. First_bonus
        5. spend_after_sftc
            0 = no
            1 = pg
            2 = non-pg
    """
    def is_bonus(x, rate):
        try:
            return int(int(x['rate']) == rate)
        except ValueError:
            return 0

    df_purchases = (df_purchases
                    .assign(
                        bonus50=df_purchases.apply(is_bonus, args=(50,), axis=1),
                        bonus18=df_purchases.apply(is_bonus, args=(18,), axis=1),
                        bonus13=df_purchases.apply(is_bonus, args=(13,), axis=1)))

    def sftc_and_after():
        res = []
        for group in df_purchases[['memberkey', 'date', 'bonus50', 'bonus18', 'bonus13', 'purchase_type']].groupby('memberkey'):
            key, df = group
            df = df[~pd.isna(df['date'])]
            if len(df) == 0:
                first_cart_bonus = -1
                post_spend_type = -1
            else:
                df_bonus = df.query('bonus50 == 1 or bonus18 == 1 or bonus13 ==1')
                if len(df_bonus) == 0:
                    first_cart_bonus = 0
                    post_spend_type = -1
                else:
                    mindate = min(df_bonus['date'])
                    first_cart = df_bonus[df_bonus['date'] == mindate]
                    first_cart_bonus = 0
                    for rate in (50, 18, 13):
                        if sum(first_cart[f'bonus{rate}']) > 0:
                            first_cart_bonus = rate
                            break

                    post_carts = df[df['date'] > mindate]
                    if len(post_carts) == 0:
                        post_spend_type = 0
                    elif sum(post_carts['purchase_type'] == 1) > 0:
                        post_spend_type = 1
                    else:
                        post_spend_type = 2

            res.append((key, first_cart_bonus, post_spend_type))
        return pd.DataFrame(res, columns=['key', 'first_bonus', 'post_spend_type'])

    df_agg = (df_purchases.groupby("memberkey")
              .agg({
                'purchase_type': 'max',
                'bonus50': 'max',
                'bonus18': 'max',
                'bonus13': 'max'
              }))

    df_sftc_and_after = sftc_and_after()

    df_new_feats = pd.merge(df_sftc_and_after, df_agg, how='left',
                            left_on='key', right_index=True)
    return pd.merge(df_members, df_new_feats, how='outer', on='key')


# Labelling type of purchaser
# 0. None
# 1. SFTC
# 2. PG purchaser
# 3. Any PG competitor purchaser
def label_spend_type_single(df_group: pd.DataFrame) -> int:
    if sum(df_group['purchase_type'] == 1) > 0:
        if sum(df_group['reward_info:reward_1:rate'].isin([13, 18, 50])) > 0:
            return 1
        else:
            return 2
    elif sum(df_group['purchase_type'] == 2):
        return 3
    else:
        return 0


def label_spend_types(df_joined):
    spend_types = df_joined.groupby('base_fields:key').apply(label_spend_type_single)
    spend_types.name = 'spend_type'
    return spend_types


def rename_purchase_cols(df_purchases):
    df_purchases = df_purchases.rename(mapper={
            "price (positive)": "price",
            "extra.matched_asins": "matched_asins"
        })
    return df_purchases


def main():
    df_members, df_joined = load_data()
    df_members = rename_cols(df_members)
    df_members = label_crm_members(df_members)
    df_joined = rename_cols(df_joined)
    df_members = subset_cols(df_members)
    df_members = format_vars(df_members)
    df_members = generate_member_features(df_members, df_joined)
    df_joined = rename_purchase_cols(df_joined)
    df_members.to_csv(config.processed / 'member_features.csv')
    df_joined.to_csv(config.processed / 'purchase_features.csv')


if __name__ == "__main__":
    main()
