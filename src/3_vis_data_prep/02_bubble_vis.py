import pandas as pd

from sftc import config, utils


def load_data():
    df_members = pd.read_csv(config.processed / 'member_data.csv')
    return df_members


def subset_cols(df):
    return df[[
        'key',
        'gender',
        'age',
        'crm',
        'any_pg',
        'sftc',
        'post_purchase',
        'total_spent'
    ]]


def main():
    pipe = [load_data, subset_cols]
    res = utils.execute_pipeline(pipe)
    res.to_csv(config.final / 'bubble_data.csv', index=False)


if __name__ == '__main__':
    main()
