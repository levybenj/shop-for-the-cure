import pandas as pd

from sftc import config, utils


def load_data():
    df_members = pd.read_csv(config.processed / 'member_data.csv')
    df_subjects = pd.read_csv(config.raw / 'user_journey_info.csv')
    return df_members, df_subjects


def join_dfs(data):
    df_members, df_subjects = data
    return pd.merge(df_subjects, df_members, how='outer', on='key')


def main():
    pipe = [load_data, join_dfs]
    res = utils.execute_pipeline(pipe)
    res.to_csv(config.final / 'user_journey_data.csv', index=False)


if __name__ == '__main__':
    main()
