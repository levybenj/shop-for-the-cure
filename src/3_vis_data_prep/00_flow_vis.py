import json

import pandas as pd
import numpy as np

from sftc import config, utils


def load_data():
    return pd.read_csv(config.processed / 'member_data.csv')


def filter_rows(df):
    return df.query('sftc in [0, 13, 18, 50] or \
                     crm == 1')
    return df[df['sftc'].isin([13, 18, 50])]


def select_cols(df):
    return df[[
        'crm',
        'sftc',
        'post_purchase',
        'any_pg',
        'total_spent'
    ]]


def process_data(df):
    df['any_purchase'] = df.apply(any_purchase, axis=1)
    df = df.drop(['any_pg', 'total_spent'], axis=1)
    df = df.fillna('none')
    for c in df.columns:
        if c in ['crm', 'sftc']:
            df[c] = df[c].apply(lambda x: c + str(int(x)))
        else:
            df[c] = df[c].apply(lambda x: c + '_' + x)

    return df


def any_purchase(row):
    if row['any_pg'] == 1:
        return 'pg'
    elif row['total_spent'] > 0:
        return 'other'
    else:
        return 'none'


def count_values(df, colname):
    vals = (df[colname].value_counts()
                       .to_frame()
                       .reset_index()
                       .rename(columns={colname: 'value'}))
    vals.name = colname
    return vals


def cols_to_nodes(df):
    colnames = ['source', 'target', 'value']

    levels1 = (df.query('sftc != "sftc0"')
                 .groupby('crm')
                 .apply(count_values, 'sftc')
                 .reset_index()
                 .drop(['level_1'], axis=1))
    levels2 = (df.query('sftc != "sftc0"')
                 .groupby('sftc')
                 .apply(count_values, 'post_purchase')
                 .reset_index()
                 .drop(['level_1'], axis=1))
    levels3 = (df.query('sftc == "sftc0"')
                 .groupby('crm')
                 .apply(count_values, 'any_purchase')
                 .reset_index()
                 .drop(['level_1'], axis=1))
    levels3['index'] = levels3['index'].apply(lambda x: x.replace('any', 'post'))

    levels1.columns = colnames
    levels2.columns = colnames
    levels3.columns = colnames

    res = pd.concat((levels1, levels2, levels3), sort=True, axis=0)
    return res


def main():
    pipe = [load_data, filter_rows, select_cols, process_data, cols_to_nodes]
    res = utils.execute_pipeline(pipe)
    res.to_csv(config.final / 'flow_data.csv', index=False)


if __name__ == '__main__':
    main()
