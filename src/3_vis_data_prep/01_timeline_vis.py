import re

import pandas as pd
import numpy as np

from sftc import config, utils


def load_data():
    df_members = pd.read_csv(config.processed / 'member_data.csv')
    df_purchases = pd.read_csv(config.processed / 'purchase_data.csv')
    return df_members, df_purchases


def process_dates(df_members, df_purchases):
    for df in df_members, df_purchases:
        colnames = [c for c in df.columns if re.search(r'date|created_at', c)]
        for col in colnames:
            df[col] = df[col].apply(utils.process_date)
    return df_members, df_purchases


def crm_by_date(df_members):
    return (df_members.groupby('created_at')
                      .agg({'crm': 'sum'})
                      .reset_index()
                      .rename(columns={
                            'created_at': 'date',
                            'crm': 'value'
                        }))


def purchase_info(df_group):
    dates, types = [], []

    mindate = min(df_group['date'])

    first_cart = df_group.loc[np.logical_and(df_group['date'] == mindate, df_group['rate'].isin([13, 18, 50]))]
    if len(first_cart) > 0:
        first_bonus = max(first_cart['rate'])
        if first_bonus in [13, 18, 50]:
            dates.append(mindate)
            types.append(f'bonus{str(int(first_bonus))}')

    post_carts = df_group.loc[np.logical_and(df_group['date'] > mindate, ~pd.isna(df_group['date']))]
    if len(post_carts) > 0:
        post_date = min(post_carts['date'])
        dates.append(post_date)
        types.append('post_pg')
    res = pd.DataFrame({
        'date': dates,
        'type': types
    })
    return res


def compute_date_counts(df_purchases):
    df_timeline = (df_purchases.groupby('memberkey')
                               .apply(purchase_info))
    df_timeline.index = pd.Series([e[0] for e in df_timeline.index])
    # Two columns: (1) date and (2) type
    return (df_timeline.assign(value=1)
                       .groupby(['date', 'type'])
                       .agg('sum')
                       .reset_index())


def split_df(df_timeline):
    df_bonus = df_timeline[df_timeline['type'].apply(lambda x: bool(re.search(r'bonus', x)))]
    df_bonus['type'] = df_bonus['type'].apply(lambda x: int(x.replace('bonus', '')))
    df_post = df_timeline[df_timeline['type'] == 'post_pg']
    return df_bonus, df_post


def main():
    df_members, df_purchases = load_data()
    df_members, df_purchases = process_dates(df_members, df_purchases)
    df_crm = crm_by_date(df_members)
    df_bonus, df_post = split_df(compute_date_counts(df_purchases))

    df_crm.to_csv(config.final / 'timeline_crm.csv', index=False)
    df_bonus.to_csv(config.final / 'timeline_bonus.csv', index=False)
    df_post.to_csv(config.final / 'timeline_post.csv', index=False)


if __name__ == '__main__':
    main()
