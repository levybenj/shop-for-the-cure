from datetime import datetime
import re

import pandas as pd
from tqdm import tqdm

from sftc import config, utils


tqdm.pandas()

sftc_brands = [b.strip() for b in (config.raw / 'sftc_brands.txt').open('r')]


def load_data():
    df_members = utils.load_raw_members_data()
    df_purchases = pd.read_csv(config.processed / 'purchase_data.csv')
    return df_members, df_purchases


def parse_date(data):
    df_members, df_purchases = data
    df_purchases['date'] = df_purchases['date'].apply(utils.process_date)
    df_members['created_at'] = df_members['created_at'].apply(utils.process_date)
    return df_members, df_purchases


def rename_cols(data):
    df_members, df_purchases = data
    df_members = utils.rename_cols(df_members)

    return df_members, df_purchases


def subset_cols(data):
    df_members, df_purchases = data
    df_members = df_members[[
        'key',
        'created_at',
        'gender',
        'landing_url',
        'postal_code',
        'yob',
        'is_canadian',
        'total_spent',
        'trips_started'
    ]]
    return df_members, df_purchases


def label_crm(data):
    df_members, df_purchases = data
    df_members['crm'] = df_members['landing_url'].apply(utils.is_crm_member)
    return df_members, df_purchases


def join_dfs(data):
    df_members, df_purchases = data
    return pd.merge(df_members, df_purchases,
                    how='left',
                    left_on='key', right_on='memberkey')


def label_member_groups(df):
    print("Labelling groups")
    df_groups = (df.groupby('key')
                   .progress_apply(label_groups_single)
                   .reset_index())
    return pd.merge(df, df_groups, how='left', on='key')


def label_groups_single(group):
    # Ever purchased P&G
    any_pg = int(sum(group['purchase_type'] == 'pg') > 0)

    # SFTC participation
    sftc_carts = group[group['rate'].isin((13, 18, 50))]
    if len(sftc_carts) > 0:
        sftc = max(sftc_carts['rate'])
    else:
        sftc = 0

    # Post-SFTC
    if sftc:
        post_carts = group[group['date'] > min(sftc_carts['date'])]
        if len(post_carts) == 0:
            post_sftc = 'none'
        elif sum(post_carts['purchase_type'] == 1) > 0:
            post_sftc = 'pg'
        else:
            post_sftc = 'other'
    else:
        post_sftc = 'none'

    return pd.DataFrame([(any_pg, sftc, post_sftc)],
                        columns=['any_pg', 'sftc', 'post_purchase'])


def generate_member_features(df):
    print("Generating features")
    return (df.groupby('key')
              .progress_apply(member_features_single)
              .reset_index())


def member_features_single(group):
    feats = []

    # Demographics
    feats.append(get_demos(group))

    # Groups
    feats.append(get_groups(group))

    # Spending
    feats.append(get_spending(group))

    # Brands
    feats.append(get_brands(group))

    # Categories
    feats.append(get_cats(group))

    # Retailers
    feats.append(get_retailers(group))

    # Frequency
    feats.append(get_freq(group))

    return pd.concat(feats, axis=1, sort=False)


def get_demos(group):
    demos = group[['gender', 'yob', 'postal_code', 'created_at']].iloc[0]
    age = 2020 - utils.convert_comma_to_int(demos['yob'])
    dct = {
        'gender': demos['gender'],
        'postal_code': demos['postal_code'],
        'created_at': demos['created_at']
    }
    if age <= 100:
        dct['age'] = age
    return utils.dict_to_df(dct)


def get_groups(group):
    groups = group[['crm', 'any_pg', 'sftc']].iloc[0]
    dct = {
        'crm': groups['crm'],
        'any_pg': groups['any_pg'],
        'sftc': groups['sftc'],
    }
    return utils.dict_to_df(dct)


def get_spending(group):
    sftc_purchases = group[group['rate'].isin([13, 18, 50])]
    feats = {}
    feats['total_spent'] = group['total_spent'].iloc[0]
    if len(sftc_purchases) > 0:
        feats['sftc_spending_actual'] = sum(sftc_purchases['actual_spent'])
        feats['sftc_spending_price'] = sum(sftc_purchases['price'])

        min_sftc_date = min(sftc_purchases['date'])
        post_carts = group[group['date'] > min_sftc_date]

        if len(post_carts) > 0:
            feats['post_spending_price'] = sum(post_carts['price'])
            feats['post_spending_actual'] = sum(post_carts['actual_spent'])

            if sum(post_carts['purchase_type'] == 'pg') > 0:
                feats['post_purchase'] = 'pg'
            else:
                feats['post_purchase'] = 'other'

        else:
            feats['post_purchase'] = 'none'

    return utils.dict_to_df(feats)


def get_brands(group):
    feats = {}
    group['brand'] = group['brand'].apply(lambda x: x.lower() if type(x) == str else '')
    for brand in sftc_brands:
        df_brand = group[group['brand'] == brand]
        feats[f'n_purchased_{brand}'] = len(df_brand)
        if len(df_brand) > 0:
            feats[f'actual_spent_{brand}'] = sum(df_brand['actual_spent'])

    return utils.dict_to_df(feats)


def get_cats(group):
    feats = {}
    product_groups = group.loc[group['purchase_type'] == 'pg', 'product_group'].unique()
    for prod_group in product_groups:
        df_prod_group = group[group['product_group'] == prod_group]
        feats[f'n_purchased_{prod_group}'] = len(df_prod_group)
        feats[f'actual_spent_{prod_group}'] = sum(df_prod_group['actual_spent'])
    return utils.dict_to_df(feats)


def get_retailers(group):
    feats = {}
    total_spent = sum(group['actual_spent'])
    group['advertiser'] = group['advertiser'].apply(lambda x: x if x in ['Amazon.ca', 'Walmart Canada'] else 'other')
    # group.loc[~group['advertiser'].isin(['Amazon.ca', 'Walmart Canada']), 'advertiser'] = 'other'
    for retailer in group['advertiser'].unique():
        df_retailer = group[group['advertiser'] == retailer]
        feats[f'n_trips_{retailer}'] = len(df_retailer['date'].unique())
        feats[f'total_spent_{retailer}'] = sum(df_retailer['actual_spent'])
        feats[f'pct_spent_{retailer}'] = feats[f'total_spent_{retailer}'] / total_spent
    return utils.dict_to_df(feats)


def get_freq(group):
    feats = {}

    n_trips = group.groupby(['date', 'advertiser']).ngroups
    months_active = utils.month_diff(group.iloc[0]['created_at'], datetime.now())

    if n_trips > 0:
        feats['purchases_per_month'] = n_trips / months_active
        feats['items_per_purchase'] = len(group) / n_trips
        feats['avg_cart_price'] = sum(group['price']) / n_trips

    return utils.dict_to_df(feats)


def filter_rows(df):
    print("Filtering rows")
    return df.query('any_pg == 1 or crm or sftc != 0')


def rename_all_cols(df):
    print("Renaming columns")
    df.columns = [re.sub('[ |]', '_', c.lower()) for c in df.columns]
    return df


def main():
    pipe = [load_data, rename_cols, parse_date, subset_cols, label_crm,
            join_dfs, label_member_groups, filter_rows,
            generate_member_features, rename_all_cols]
    res = utils.execute_pipeline(pipe)
    print("Saving")
    res.to_csv(config.processed / 'member_data.csv', index=False)


if __name__ == '__main__':
    main()
