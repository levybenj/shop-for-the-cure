import re

import pandas as pd

from sftc import config, utils


# Importing data
def load_data():
    df_purchases = pd.read_csv(config.data / 'raw' / 'sa_purchases_raw.csv')
    df_products = pd.read_csv(config.data / 'raw' / 'SFTC_ASIN_INFO.csv', index_col=0)  # Check this name
    df_products = df_products[['asin', 'brand', 'product_group']]
    return df_purchases, df_products


# Subsetting columns
col_re = [
    'date$',
    'advertiser',
    'cancelled_at',
    'extra.matched_asins'
    'name',
    'price$',
    'reward_1:rate',
    'base_info:rate'
    'reward_1:reward',
    'matched_asins',
    'has_bonus',
    'category_rate',
    'product_id',
    'member_id',
    'base_info:memberkey'
]


def subset_columns(data):
    df_purchases, df_products = data
    df_purchases = df_purchases[[c for c in df_purchases.columns if any([re.search(r, c) for r in col_re])]]
    return df_purchases, df_products


# Labelling any purchases with their brand
def label_brands(data):
    df_purchases, df_products = data
    df_purchases['base_info:extra.matched_asins'] = \
        df_purchases['base_info:extra.matched_asins'].apply(utils.process_asin)
    return pd.merge(df_purchases, df_products, how='left',
                    left_on='base_info:extra.matched_asins', right_on='asin')


# Labelling PG purchases
pg_brands = [b.lower() for b in utils.get_brands()]


def label_purchase_single(brand_purchased):
    if type(brand_purchased) != str:
        return 'nobrand'
    for b in pg_brands:
        if re.search(b, brand_purchased.lower()):
            return 'pg'
    # TODO: Label competitor purchases
    return 'other'


def label_purchases(df_purchases):
    df_purchases['purchase_type'] = df_purchases['brand'].apply(label_purchase_single)
    return df_purchases


def rename_cols(df):
    return utils.rename_cols(df)


def clean_price(df):
    df['price'] = df['price'].apply(lambda x: float(re.sub(r'[,\-]', '', x)))
    return df


def compute_actual_spent(df):
    df['actual_spent'] = df['price'] * (1 - df['rate'] / 100)
    return df


def parse_date(df):
    df['date'] = df['date'].apply(utils.process_date)
    return df


def main():
    pipe = [load_data, subset_columns, label_brands, label_purchases,
            rename_cols, clean_price, compute_actual_spent, parse_date]
    res = utils.execute_pipeline(pipe)
    res.to_csv(config.processed / 'purchase_data.csv', index=False)


if __name__ == '__main__':
    main()
