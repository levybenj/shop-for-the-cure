import requests
import json

from sftc.exceptions import RequestError


ASIN_API_BASEURL = "https://shopperarmy.com/amazon_caller/api/asin/"
EXTRA_ARGS = "?timeout=-1"


def asin_api_call(asin: str) -> str:
    """Call the ASIN api to get the brand for product with matching asin.
    """
    api_url = ASIN_API_BASEURL + asin + EXTRA_ARGS
    try:
        res = requests.get(api_url)
    except requests.exceptions.ConnectionError as e:
        raise RequestError(str(e))
    try:
        amazondata = res.json().get("amazon_data", {})
        return amazondata
    except json.decoder.JSONDecodeError:
        print(asin)
        return {}
