import re
from datetime import datetime

import pandas as pd

from sftc import config


def get_brands() -> list:
    with (config.data / 'raw' / 'pg_brands.txt').open('r') as f:
        brands = [b.strip() for b in f]
    return brands


def process_asin(asin):
    try:
        return asin.split(',')[0].strip('"[]"')
    except AttributeError:
        return ''


def convert_comma_to_int(x):
    try:
        return int(x)
    except ValueError:
        try:
            return int(x.replace(",", ""))
        except AttributeError:
            return -1


def convert_comma_to_float(x):
    try:
        return float(x.replace(",", ""))
    except AttributeError:
        return -1


# CRM or not
with (config.data / 'raw' / 'landing_url_newsletters.txt').open('r') as f:
    urlpatterns = [line.strip() for line in f]


def is_crm_member(landing_url):
    try:
        return int(any([re.search(pattern, landing_url) for pattern in urlpatterns]))
    except TypeError:
        return 0


def load_raw_members_data():
    df_members = pd.concat([pd.read_csv(fname) for fname in (config.data / 'raw').glob('sa members*.csv')], sort=True)
    return df_members


def process_date(x):
    try:
        if '@' in x:
            x = x.split('@')[0].strip()
        return datetime.strptime(x, '%b %d, %Y')
    except (AttributeError, TypeError):
        return x
    except ValueError:
        return datetime.strptime(x, '%Y-%m-%d')


def rename_cols(df):
    df = df[[c for c in df.columns if not re.search(r'gt\d$', c)]]
    df.columns = [c.split(":")[-1] for c in df.columns]
    return df


def execute_pipeline(pipe):
    df = pipe[0]()
    for f in pipe[1:]:
        df = f(df)
    return df


def ser_to_df(ser):
    """
    Returns a dataframe with one row and n columns for a series of
    length n.
    """
    return ser.to_frame().T


def dict_to_df(dct):
    """
    Returns a dataframe with one row and n columns for a dict of
    length n.
    """
    return ser_to_df(pd.Series(dct))


def month_diff(d1, d2) -> float:
    """Return the approximate number of months between two dates."""
    return (d2 - d1).days / (365 / 12)
