from pathlib import Path
from datetime import datetime as dt


root = Path(__file__).parent.parent.parent
data = root / 'data'
raw = data / 'raw'
processed = data / 'processed'
final = root / 'app' / 'data'
docs = root / 'docs'
models = root / 'models'
notebooks = root / 'notebooks'
src = root / 'src'
tests = root / 'tests'
results = root / 'results'

START_DATE = dt.strptime('2019-10-01', '%Y-%m-%d')
END_DATE = dt.strptime('2019-11-30', '%Y-%m-%d')
