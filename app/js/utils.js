function makeSvg(vis, clip) {
  var svg = d3.select('#' + vis.parentElement)
      .append('div')
      .attr('class', 'svg-container')
      .append('svg')
      .attr('height', vis.height + vis.margin.top + vis.margin.bottom)
      .attr('width', vis.width + vis.margin.left + vis.margin.right)
      .append('g')
      .attr('transform', 'translate(' + vis.margin.left + ',' + vis.margin.top + ')');

  // SVG clipping path
  if (clip === undefined) {
    clip = true;
  }
  if (clip) {
    svg.append('defs')
        .append('clipPath')
        .attr('id', 'clip')
        .append('rect')
        .attr('width', vis.width)
        .attr('height', vis.height);
  }
  return svg;
}

var parseTime = d3.timeParse("%b %d, %Y @ %H:%M:%S.%L");
var parseDate = d3.timeParse("%Y-%m-%d");

var aggFuncs = {
  sum: d3.sum,
  count: function(d) { return d.length; },
  prop: function(d) { return d3.sum(d) / d.length; },
  mean: d3.mean,
  first: function(d) { return d[0]; }
};

function inRange(x, r) {
  return r[0] <= x && x <= r[1];
}

function wrap(text, width) {
  text.each(function() {
    var text = d3.select(this),
        words = text.text().split(/\s+/).reverse(),
        word,
        line = [],
        lineNumber = 0,
        lineHeight = 1.1, // ems
        x = text.attr('x'),
        y = text.attr("y"),
        dy = 0,
        tspan = text.text(null).append("tspan").attr("x", x).attr("y", y).attr("dy", dy + "em");
    while (word = words.pop()) {
      line.push(word);
      tspan.text(line.join(" "));
      if (tspan.node().getComputedTextLength() > width) {
        line.pop();
        tspan.text(line.join(" "));
        line = [word];
        tspan = text.append("tspan").attr("x", x).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
      }
    }
  });
}

function dateDiff(d1, d2) {
  const dayLength = 1000 * 60 * 60 * 24;
  var diff = Math.round(d2.getTime() - d1.getTime()) / dayLength;
  return diff.toFixed(0);
}

function loadCsvFiles(datasets, f) {
  var promises = [];
  datasets.forEach(url => promises.push(d3.csv(url)));

  Promise.all(promises).then(function(data) {
    f(data);
  });
}

function unpackConfig(vis) {
  vis.svg = vis.config.svg;
  vis.height = vis.config.height;
  vis.width = vis.config.width;
  vis.margin = vis.config.margin;
  vis.hidden = vis.config.hidden;
}

function applyFilter(data, filter, displayVars) {
  console.log(filter);
  return data.filter(d => {
    return displayVars.every(e => !isNaN(d[e])) && filter.every(e => {
      if (e.varname === 'n_purchased') {
        return e.values.some(brand => {
          return d[e.varname + '_' + brand] > 0;
        })
      } else {
        return e.values.some(val => {
          return val == d[e.varname];
        })
      }
    })
  })
}
function highlight(vis, key) {
  vis.markers.classed('highlight', d => {
    console.log(d)
    if (d.key === key) {
      return true;
    }
  });
}
function resetHighlight(vis) {
  vis.markers.classed('highlight', false);
}