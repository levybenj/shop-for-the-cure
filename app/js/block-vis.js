BlockVis = function(_parentElement, _data, _config) {
  this.parentElement = _parentElement;
  this.data = _data;
  this.config = _config;

  this.initVis();
};
BlockVis.prototype.initVis = function() {
  var vis = this;

  if (!vis.parentElement) {
    unpackConfig(vis);
  }
  // Additional settings
  vis.blockLength = 20;
  vis.padding = 5;

  vis.gBlocks = vis.svg.append('g').attr('class', 'blocks');
  vis.gAnnotations = vis.svg.select('g.annotations');
  vis.xAxis = vis.svg.append('g').attr('class', 'x axis');

  // Color scale for blocks
  vis.keys = [
    'health_and_beauty',
    'ivory',
    'pantene',
    'tide'];
  vis.scaleColor = d3.scaleOrdinal()
      .domain(vis.keys)
      .range(['var(--sa-gray)', 'var(--ivory-pink)', 'var(--pantene-beige)', 'var(--tide-orange)']);

  vis.wrangleData();
};
BlockVis.prototype.wrangleData = function() {
  var vis = this;

  vis.displayData = [];
  vis.data.forEach(function(d) {
    var blocks = [],
        i = 0;
    vis.keys.forEach(function(k) {
      var n = +d['n_purchased_' + k],
          ncols = 4;
      d3.range(n).forEach(function() {
        blocks.push({
          'type': k,
          'row': Math.floor(i / ncols),
          'col': i % ncols
        });
        i++;
      });
    });
    vis.displayData.push(blocks);
  });

  console.log(vis.displayData);

};
BlockVis.prototype.updateVis = function() {
  var vis = this;

  vis.gBlocks.selectAll('g.member')
      .data(vis.displayData)
      .enter()
        .append('g')
        .attr('class', 'member')
        .attr('transform', (d, i) => 'translate(' + ((i + 1) *  vis.width / 3 - 2 * (vis.blockLength + vis.padding)) + ',0)')
      .selectAll('rect.block')
        .data(d => d)
      .enter()
        .append('rect')
        .attr('class', 'block')
        .attr('x', d => d.col * (vis.blockLength + vis.padding))
        .attr('y', d => vis.height - d.row * (vis.blockLength + vis.padding))
        .attr('height', vis.blockLength)
        .attr('width', vis.blockLength)
        .style('fill', d => vis.scaleColor(d.type))
        .style('opacity', 0)
      .transition()
        .duration(500)
        .delay((d, i) => i * 100 + 500)
        .style('opacity', 1)


};
BlockVis.prototype.render = function() {
  var vis = this;

  vis.xAxis.selectAll('line')
      .transition()
      .duration(1000)
      .style('opacity', 1);

  vis.updateVis();
};
BlockVis.prototype.cleanUp = function() {
};