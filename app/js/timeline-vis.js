/**
 * TimelineVis -- Object constructor function
 * @param _parentElement
 * @param _data
 * @param _config
 * @param _eventHandler
 * @constructor
 */
TimelineVis = function(_parentElement, _data, _config, _eventHandler) {
  this.parentElement = _parentElement;
  this.data = _data;
  this.displayData = _data;
  this.config = _config;
  this.eventHandler = _eventHandler;

  this.initVis();
};

TimelineVis.prototype.initVis = function() {
  var vis = this;

  vis.margin = {
    top: 60,
    bottom: 20,
    left: 30,
    right: 20
  };
  vis.width = $('#' + vis.parentElement).width() - vis.margin.left - vis.margin.right;
  vis.height = vis.width * .4;
  vis.svg = makeSvg(vis);

  // Constants
  vis.barPadding = 0.5;
  // Idk why, but 9 == October...
  vis.minDate = new Date(2019, 9, 1);
  vis.maxDate = new Date(2019, 10, 30);

  const nDates = dateDiff(vis.minDate, vis.maxDate);

  vis.barWidth = vis.width / nDates - 2 * vis.barPadding;

  // Filtering all data
  function filterDate(d) {
    // return true;
    return d.date >= vis.minDate && d.date <= vis.maxDate;
  }
  vis.data.crm = vis.data.crm.filter(filterDate);
  vis.data.bonus = vis.data.bonus.filter(filterDate);
  vis.data.post = vis.data.post.filter(filterDate);

  // Selected variable
  vis.selected = vis.config.selected || 'crm';

  vis.gBars = vis.svg.append('g');

  vis.x = d3.scaleTime()
      .domain([vis.minDate, vis.maxDate])
      .range([0, vis.width]);

  vis.y = d3.scaleLinear().range([vis.height, 0]);

  vis.xAxis = d3.axisBottom().scale(vis.x);
  vis.yAxis = d3.axisLeft();

  vis.gX = vis.svg.append('g')
      .attr('class', 'x axis')
      .attr('transform', 'translate(0,' + vis.height + ')');
  vis.gY = vis.svg.append('g')
      .attr('class', 'y axis');

  // Just call the xAxis
  vis.gX.call(vis.xAxis);

  // Drawing the name of the variable as the title
  vis.title = vis.svg.append('text')
      .attr('x', vis.width)
      .attr('y', 20)
      .attr('text-anchor', 'end')
      .attr('class', 'title');

  // Creating a dictionary to lookup the titles
  vis.titleLookup = {
    crm: 'Accounts created after referral from P&G newsletters',
    bonus: 'First time participating in Shop for the Cure',
    post: 'Purchases of products through Shopper Army after SFTC'
  };

  // Creating a list for selecting the variables
  vis.levels = ['crm', 'bonus', 'post'];

  // Annotations for email dates
  vis.emails = [
    {name: "P&G Everyday", date: new Date(2019, 9, 7)},
    {name: "Shopper's Voice", date: new Date(2019, 9, 18)}
  ];
  vis.annots = vis.svg.append('g')
      .attr('class', 'emails')
      .selectAll('g.email')
      .data(vis.emails)
      .enter()
      .append('g')
      .attr('class', 'email')
      .attr('transform', d => 'translate(' + vis.x(d.date) + ',0)');

  vis.annots.append('line')
      .attr('class', 'dashed')
      .attr('x1', 0)
      .attr('x2', 0)
      .attr('y1', -10)
      .attr('y2', vis.height);

  vis.annots.append('text')
      .attr('class', 'label')
      .text(d => d.name)
      .attr('x', 0)
      .attr('y', -20);

  vis.wrangleData();
};
TimelineVis.prototype.wrangleData = function() {
  var vis = this;

  vis.displayData = vis.data[vis.selected];

  vis.updateVis();
};
TimelineVis.prototype.updateVis = function() {
  var vis = this;

  // Update y scale
  vis.y.domain([0, d3.max(vis.displayData, d => d.value)]);
  vis.yAxis.scale(vis.y);

  // Call the y axis
  vis.gY.call(vis.yAxis);

  // Draw rectangles
  let bars = vis.svg.selectAll('rect.bar')
      .data(vis.displayData);

  bars.exit().remove();

  bars.enter()
        .append('rect')
        .attr('class', 'bar')
      .merge(bars)
        .transition()
        .attr('x', d => vis.x(d.date) - vis.barWidth / 2)
        .attr('y', d => vis.y(d.value))
        .attr('height', d => vis.y(0) - vis.y(d.value))
        .attr('width', vis.barWidth);

  vis.title.text(vis.titleLookup[vis.selected]);
};

TimelineVis.prototype.selectionChanged = function(depth) {
  var vis = this;

  if (depth < 2) {
    vis.selected = vis.levels[depth];
    vis.wrangleData();
  }
};
TimelineVis.prototype.render = function() {
  console.log("Rendering timeline!");
};