/**
 * JourneyVis -- Object constructor function
 *
 * Object for managing the 2-user journey part of the visual story.
 *
 * @param _parentElement
 * @param _data
 * @param _config
 * @constructor
 */
JourneyVis = function(_parentElement, _data, _config) {
  this.parentElement = _parentElement;
  this.data = _data;

  this.initVis()
};
JourneyVis.prototype.initVis = function() {
  var vis = this;

  // Setup height, width, SVG
  // Setup
  vis.margin = {
    top: 60,
    bottom: 100,
    left: 60,
    right: 20
  };
  vis.width = $('#' + vis.parentElement).width() - vis.margin.left - vis.margin.right;
  vis.height = vis.width * .5;
  vis.svg = makeSvg(vis);

  vis.svg.append('g').attr('class', 'markers');
  vis.svg.append('g').attr('class', 'annotations');

  // Config
  vis.config = {
    svg: vis.svg,
    height: vis.height,
    width: vis.width,
    margin: vis.margin,
    hidden: true
  };

  vis.filteredData = vis.data.journey.filter(d => d.name !== "");

  // Setup each vis
  vis.bubbleVis = new BubbleVis(null, vis.data.bubble, vis.config);
  vis.blockVis = new BlockVis(null, vis.filteredData, vis.config);
  vis.scatterVis = new ScatterVis(null, vis.filteredData, vis.config);
  vis.triangleVis = new TriangleVis(null, vis.filteredData, vis.config);

  vis.wrangleData();
};
JourneyVis.prototype.wrangleData = function() {
  var vis = this;

  vis.displayData = vis.data.journey;
};
JourneyVis.prototype.renderBubbleVis = function() {
  console.log("Rendering bubbleVis");

  var vis = this;

  vis.bubbleVis.render();
};
JourneyVis.prototype.filterData = function() {
  var vis = this;

  // vis.bubbleVis.force.stop();
  vis.displayData = vis.filteredData;

  // Manipulate the DOM elements directly here... keeps it simple
  var markers = d3.select('g.markers').selectAll('circle.marker')
      .data(vis.displayData, d => d.key);

  markers.exit()
      .transition()
      .duration(1000)
      .style('opacity', 0)
      .on('end', function() {
        d3.select(this).remove();
      });

  markers.transition()
      .duration(1500)
      .attr('cx', (d, i) => vis.width / 3 * (i + 1))
      .attr('cy', vis.height / 2)
      .attr('r', 10);

  vis.svg.select('g.labels').selectAll('text')
      .transition()
      .duration(500)
      .style('opacity', 0)
      .on('end', function() {
        d3.select(this).remove();
      });
};
JourneyVis.prototype.showInfo = function() {
  var vis = this;

  console.log("Showing info");

  var infoText = [
      "Alex (from CRM)",
      "Jaime (already on SA)"
  ];
  vis.svg.select('g.labels').selectAll('text')
      .data(infoText)
      .enter()
      .append('text')
      .attr('class', 'label name')
      .attr('x', (d, i) => vis.width / 3 * (i + 1))
      .attr('y', vis.height / 2 + 50)
      .text(d => d)
      .style('text-anchor', 'middle')
      .style('opacity', 0)
      .transition()
      .duration(500)
      .delay((d, i) => i * 200)
      .style('opacity', 1);

  vis.svg.select('g.markers')
      .transition()
      .duration(500)
      .attr('transform', 'translate(0,0)');

  vis.svg.select('g.labels')
      .transition()
      .duration(500)
      .attr('transform', 'translate(0,0)');

  vis.cleanUpBlockVis();
};
JourneyVis.prototype.renderBlockVis = function() {
  var vis = this;

  vis.cleanUpScatterVis();

  vis.svg.select('g.markers')
      .transition(500)
      .attr('transform', 'translate(0,' + (vis.height / 2 + 40) + ')')
      .selection()
      .selectAll('circle.marker')
      .data(vis.displayData, d => d.key)
      .transition()
      .duration(500)
      .attr('cx', (d, i) => vis.width / 3 * (i + 1))
      .attr('cy', vis.height / 2)
      .attr('r', 10);

  vis.svg.select('g.labels')
      .transition()
      .duration(500)
      .attr('transform', 'translate(0,' + (vis.height / 2 + 40) + ')')
      .style('opacity', 1);

  vis.svg.select('g.axes')
      .transition()
      .duration(500)
      .style('opacity', 0)
      .on('end', function() { d3.select(this).remove(); });

  vis.blockVis.render();
};
JourneyVis.prototype.renderScatterVis = function() {
  var vis = this;

  vis.cleanUpTriangleVis();
  vis.cleanUpBlockVis();
  vis.svg.select('g.markers')
      .transition()
      .duration(500)
      .attr('transform', 'translate(0,0)');

  vis.scatterVis.render();

  vis.svg.select('g.labels')
      .transition(500)
      .attr('transform', 'translate(-10,-10)')
      .selection()
      .selectAll('text.label')
      .data(vis.displayData)
      .text(d => d.name)
      .style('text-anchor', 'end')
      .transition(500)
      .attr('x', d => vis.scatterVis.x(d.age))
      .attr('y', d => vis.scatterVis.y(d.total_spent));

  // vis.svg.select('g.labels')
  //     .transition()
  //     .duration(500)
  //     .style('opacity', 0);
};
JourneyVis.prototype.renderTriangleVis = function() {
  var vis = this;

  vis.cleanUpScatterVis();
  vis.triangleVis.render();

  vis.svg.select('g.labels')
      .selectAll('text.label')
      .data(vis.displayData)
      .text(d => d.name)
      .style('text-anchor', 'start')
      .transition(500)
      .attr('transform', 'translate(20,-10)')
      .attr('x', d => xCoord(d, vis.triangleVis))
      .attr('y', d => yCoord(d, vis.triangleVis));
};

JourneyVis.prototype.cleanUpScatterVis = function() {
  var vis = this;

  vis.svg.select('g.axes')
      .transition()
      .duration(500)
      .style('opacity', 0)
      .on('end', function() { d3.select(this).remove(); })
      .remove();
};
JourneyVis.prototype.cleanUpBlockVis = function() {
  var vis = this;
  vis.svg.selectAll('g.member')
      .transition()
      .duration(1000)
      .style('opacity', 0)
      .on('end', function() { d3.select(this).remove(); });
};
JourneyVis.prototype.cleanUpTriangleVis = function() {
  var vis = this;

  vis.svg.select('g.border')
      .transition(500)
      .style('opacity', 0)
      .on('end', function() { d3.select(this).remove(); });
};