/**
 * ScatterVis - Object constructor function
 *
 * Generic scatterplot
 *
 * @param _parentElement
 * @param _data
 * @param _config
 * @constructor
 */
ScatterVis = function(_parentElement, _data, _config) {
  this.parentElement = _parentElement;
  this.data = _data;
  this.displayData = _data;
  this.config = _config;

  if (!this.config.hidden) {
    this.initVis();
  }
};

ScatterVis.prototype.initVis = function() {
  var vis = this;

  if (!vis.parentElement) {
    unpackConfig(vis);
    vis.gMarkers = vis.svg.select('g.markers');
    vis.gAnnotations = vis.svg.select('g.annotations');
  } else {
    vis.margin = {
      top: 40,
      bottom: 40,
      left: 40,
      right: 40
    };
    vis.width = $('#' + vis.parentElement).width() - vis.margin.left - vis.margin.right;
    vis.height = 300;
    vis.svg = makeSvg(vis);
    // Groups for future components
    vis.gMarkers = vis.svg.append('g').attr('class', 'markers');
    vis.gAnnotations = vis.svg.append('g').attr('class', 'annotations');

    //TODO: Adding selectboxes for choosing the variable

    console.log("Making a new SVG");
  }


  // Initializing scales and axes
  vis.x = d3.scaleLinear().range([10, vis.width]);
  vis.y = d3.scaleLinear().range([vis.height - 10, 0]);
  vis.scaleRadius = d3.scaleSqrt().range([5, 10]);
  vis.scaleColorBonus = d3.scaleSequential(d3.interpolateReds);

  vis.xAxis = d3.axisBottom()
      .scale(vis.x);
  vis.yAxis = d3.axisLeft()
      .scale(vis.y);

  vis.gAxes = vis.svg.append('g')
      .attr('class', 'axes')
      .style('opacity', vis.config.hidden ? 0 : 1);

  vis.gX = vis.gAxes
      .append('g')
      .attr('class', 'x axis')
      .attr('transform', 'translate(0,' + vis.height + ')');
  vis.gY = vis.gAxes
      .append('g').attr('class', 'y axis');

  // Initial x and y variables
  vis.xVar = vis.config.initXVar || {varname: 'age', displayName: 'Age'};
  vis.yVar = vis.config.initYVar || {varname: 'total_spent', displayName: 'Total Spent (CAD)'};
  vis.filter = vis.config.initFilter || [];

  vis.xLabel = vis.gX.append('text')
      .attr('text-anchor', 'end')
      .attr('class', 'axis-label x big')
      .attr('x', vis.width);
  vis.yLabel = vis.gY.append('text')
      .attr('class', 'axis-label y big');

  vis.wrangleData();
};

ScatterVis.prototype.wrangleData = function() {
  var vis = this;

  vis.displayData = applyFilter(vis.data, vis.filter, [vis.xVar.varname, vis.yVar.varname]);
  console.log(vis.displayData);
  vis.updateVis();
};
ScatterVis.prototype.updateVis = function() {
  var vis = this;

  // Update scales
  vis.x.domain([0, d3.max(vis.displayData, d => d[vis.xVar.varname])]);
  vis.y.domain([0, d3.max(vis.displayData, d => d[vis.yVar.varname])]);
  vis.scaleRadius.domain(d3.extent(vis.displayData, d => d.total_spent));

  // Axes
  vis.xAxis.scale(vis.x);
  vis.yAxis.scale(vis.y);

  vis.gX.call(vis.xAxis);
  vis.gY.call(vis.yAxis);

  // Markers
  vis.markers = vis.gMarkers.selectAll('circle.marker')
      .data(vis.displayData, d => d.key);

  vis.markers
      .enter()
        .append('circle')
        .attr('class', 'marker')
        .classed('dashboard', !vis.config.hidden)
      .merge(vis.markers)
        .transition()
        .duration(1000)
        .attr('r', d => vis.scaleRadius(d.total_spent))
        .attr('cx', d => vis.x(d[vis.xVar.varname]))
        .attr('cy', d => vis.y(d[vis.yVar.varname]))
      .selection()
      .on('mouseover', function(d) {
        d3.select(this).classed('highlight', true);
        triangleVis.highlight(d.key);
        // dbEventHandler.highlight(d.key);
      })
      .on('mouseout', function() {
        d3.select(this).classed('highlight', false);
        triangleVis.resetHighlight();
        // dbEventHandler.resetHighlight();
      });

  vis.markers.exit()
      .transition()
      .style('opacity', 0)
      .on('end', function() { d3.select(this).remove(); });


  // Axis labels
  vis.xLabel.text(vis.xVar.displayName);
  vis.yLabel.text(vis.yVar.displayName);

  vis.gAxes
      .transition()
      .delay(500)
      .style('opacity', 1)
};

ScatterVis.prototype.applyFilter = function(filter) {
  var vis = this;

  vis.filter = filter;
  vis.wrangleData();
};

ScatterVis.prototype.render = function() {
  this.initVis();
};
ScatterVis.prototype.changeX = function(xVar) {
  var vis = this;
  vis.xVar = xVar;
  vis.wrangleData();
};
ScatterVis.prototype.changeY = function(yVar) {
  var vis = this;
  vis.yVar = yVar;
  vis.wrangleData();
};
ScatterVis.prototype.highlight = function(key) {
  this.markers.classed('highlight', d => {
    console.log(d);
    return d.key === key;
  });
};