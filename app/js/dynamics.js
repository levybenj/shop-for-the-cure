dbEventHandler = {
  targets: [scatterVis, triangleVis],
  update: function() {
    // Make the filter object
    console.log("Updating");
    var filter = [];

    $.each($('div.form-group').not('.var-selector'), function() {
      var varname = $($(this).find('label.control-label')[0]).attr('for'),
          values = [];

      console.log(varname);

      if (varname) {
        var sel;
        if (varname === 'n_purchased') {
          sel = `select[name='${varname}'] option:selected`;
          console.log(sel);
        } else {
          sel = `input[name='${varname}']:checked`;
        }
        $(sel).each(function() {
          values.push($(this).val());
        });
        filter.push({
          varname: varname,
          values: values
        });
      }
    });

    console.log(filter);

    scatterVis.applyFilter(filter);
    triangleVis.applyFilter(filter);
  },
  highlight: function(d) {
    scatterVis.highlight(d.key);
    // highlight(scatterVis, d.key);
    highlight(triangleVis, d.key);
  },
  resetHighlight:  function() {
    resetHighlight(scatterVis);
    resetHighlight(triangleVis);
  }
};

timeEventHandler = {};
$(timeEventHandler).bind("levelChanged", function(event, depth) {
  timelineVis.selectionChanged(depth);
});

$('#updateFilter').on('click', function() {
  dbEventHandler.update();
});

$("#xvar").change(function() {
  var selected = $(this).children('option:selected'),
      varname = selected.val(),
      displayName = selected.text();
  scatterVis.changeX({varname: varname, displayName: displayName});
});

$("#yvar").change(function() {
  var selected = $(this).children('option:selected'),
      varname = selected.val(),
      displayName = selected.text();
  scatterVis.changeY({varname: varname, displayName: displayName});
});