const datadir = 'data/';

// Globals
var dbEventHandler,
    timeEventHandler,
    scatterVis,
    flowVis,
    timelineVis,
    triangleVis,
    journeyVis;

// flowVis
d3.csv(datadir + 'flow_data.csv')
    .then(createFlowVis);

function createFlowVis(data) {
  data.forEach(function(d) {
    d.value = +d.value;
  });
  var config = {},
      flowData = {
        links: data,
        nodes: [
          {
            "name": "crm0",
            "displayName": "Shopper Army"
          },
          {
            "name": "crm1",
            "displayName": "P&G CRM"
          },
          {
            "name": "sftc0",
            "displayName": "No purchase"
          },
          {
            "name": "sftc50",
            "displayName": "50% bonus"
          },
          {
            "name": "sftc18",
            "displayName": "18% bonus"
          },
          {
            "name": "sftc13",
            "displayName": "13% bonus"
          },
          {
            "name": "post_purchase_pg",
            "displayName": "P&G"
          },
          {
            "name": "post_purchase_none",
            "displayName": "None"
          },
          {
            "name": "post_purchase_other",
            "displayName": "Other"
          }
        ]
      };

  flowVis = new FlowVis('flow-vis', flowData, config, timeEventHandler);

  gs1 = d3.graphScroll()
      .container(d3.select('#container-1'))
      .graph(d3.selectAll('#container-1 > .graph'))
      .sections(d3.selectAll('#container-1 > .sections > div'))
      .eventId('eventId1')
      .on('active', function(i) {
        console.log('GS1 Section ' + i);
        callbacks1[i]();
      });
}

// timelineVis
var timelineFiles = ["timeline_crm.csv", "timeline_bonus.csv", "timeline_post.csv"].map(d => datadir + d);
loadCsvFiles(timelineFiles, createTimelineVis);

function createTimelineVis(datasets) {
  datasets.forEach(function(dataset) {
    dataset.forEach(function(d) {
      d.date = parseDate(d.date);
      d.value = +d.value;
      if (d.hasOwnProperty('type')) {
        d.type = +d.type;
      }
    })
  });
  var data = {'crm': datasets[0], 'bonus': datasets[1], 'post': datasets[2]};
  timelineVis = new TimelineVis('timeline-vis', data, {}, timeEventHandler);
}

// journeyVis
var journeyFiles = [
  d3.csv(datadir + "user_journey_data.csv"),
  d3.json(datadir + "bubble_data.json")
];
Promise.all(journeyFiles).then(function(datasets) {
  var journeyData = datasets[0],
      bubbleData = datasets[1];

  journeyData.forEach(d => {
    d.age = (d.age >=10) ? +d.age : null;
    d.crm = +d.crm;
    d.any_pg = +d.any_pg;
    d.sftc = +d.sftc;
    d.total_spent = +d.total_spent;

    d.pct_spent_amazon = +d['pct_spent_amazon.ca'];
    d.pct_spent_walmart = +d.pct_spent_walmart_canada;
    d.pct_spent_other = +d.pct_spent_other;
  });

  bubbleData.forEach(d => {
    d.x = +d.x;
    d.y = +d.y;
    d.vx = +d.vx;
    d.vy = +d.vy;
    d.crm = +d.crm;
    d.sftc = +d.sftc
  });

  createJourneyVis(journeyData, bubbleData);
  createScatterVis(journeyData);
  createTriangleVis(journeyData);
});

// d3.csv(datadir + 'user_journey_data.csv')
//     .then(function(data) {
//       // Check these variables
//       data.forEach(d => {
//         d.age = +d.age;
//         d.crm = +d.crm;
//         d.any_pg = +d.any_pg;
//         d.sftc = +d.sftc;
//         d.total_spent = +d.total_spent;
//
//         d.pct_spent_amazon = +d['pct_spent_amazon.ca'];
//         d.pct_spent_walmart = +d.pct_spent_walmart_canada;
//         d.pct_spent_other = +d.pct_spent_other;
//       });
//       createJourneyVis(data);
//       createScatterVis(data);
//       createTriangleVis(data);
//     });

function createJourneyVis(journeyData, bubbleData) {

  data = {
    journey: journeyData,
    bubble: bubbleData
  };

  journeyVis = new JourneyVis('container-2 > .graph', data);

  gs2 = d3.graphScroll()
      .container(d3.select('#container-2'))
      .graph(d3.selectAll('#container-2 > .graph'))
      .sections(d3.selectAll('#container-2 > .sections > div'))
      .eventId('eventId2')
      .on('active', function(i) {
        console.log('GS 2 section ' + i);
        callbacks2[i]();
      });
}

// DASHBOARD STUFF
// scatterVis

function createScatterVis(data) {
    scatterVis = new ScatterVis('scatter-vis', data, {}, dbEventHandler);
}

// triangleVis
function createTriangleVis(data) {
  triangleVis = new TriangleVis('triangle-vis', data, {}, dbEventHandler);
}