TriangleVis = function(_parentElement, _data, _config, _eventHandler) {
  this.parentElement = _parentElement;
  this.data = _data;
  this.config = _config;
  this.eventHandler = _eventHandler;

  if (!this.config.hidden) {
    this.initVis();
  }
};
TriangleVis.prototype.initVis = function() {
  var vis = this;

  if (!vis.parentElement) {
    unpackConfig(vis);

    vis.gMarkers = vis.svg.select('g.markers');
    vis.gLabels = vis.svg.select('g.labels');
    vis.gAnnotations = vis.svg.select('g.annotations');
  } else {
    vis.margin = {
      top: 40,
      bottom: 200,
      left: 120,
      right: 100
    };
    vis.width = $('#' + vis.parentElement).width() - vis.margin.left - vis.margin.right;
    vis.height = 300;
    vis.svg = makeSvg(vis);
    // Groups for future components
    vis.gMarkers = vis.svg.append('g').attr('class', 'markers');
    vis.gAnnotations = vis.svg.append('g').attr('class', 'annotations');
    vis.gLabels = vis.svg.append('g').attr('class', 'labels')
  }

  vis.filter = vis.config.initFilter || [];

  // Any additional constants not in config
  vis.length = Math.min(vis.width, Math.sqrt(3) * vis.height / 2);

  // Initialize scales
  vis.theta = d3.scaleLinear()
      .domain([0, 1])
      .range([0, Math.PI / 3]);
  vis.x = d3.scaleLinear()
      .domain([0, 1])
      .range([(vis.width - vis.length) / 2, (vis.width + vis.length) / 2]);
  vis.y = d3.scaleLinear()
      .domain([0, Math.sqrt(3) / 2])
      .range([vis.height, vis.height - vis.length * Math.sqrt(3) / 2]);
  vis.scaleRadius = d3.scaleSqrt().range([5, 10]);

  var vertices = [
    {
      name: 'Amazon',
      pct_spent_amazon: 1,
      pct_spent_other: 0,
      pct_spent_walmart: 0
    },
    {
      name: 'Wal-Mart',
      pct_spent_amazon: 0,
      pct_spent_other: 0,
      pct_spent_walmart: 1
    },
    {
      name: 'Other',
      pct_spent_amazon: 0,
      pct_spent_other: 1,
      pct_spent_walmart: 0
    }
  ];

  var borderLines = vertices.map(function(d, i) {
    var next = (i < vertices.length - 1) ? vertices[i + 1] : vertices[0];
    return {
      source: d,
      target: next
    }
  });


  vis.gBorder = vis.svg.append('g')
      .attr('class', 'border')
      .attr('transform-origin', `${vis.width / 2} ${vis.height - (vis.length * Math.sqrt(3) / 6)}`)
      .attr('transform', 'scale(1.1)')
      .style('opacity', 0)
      .transition()
      .duration(vis.config.hidden ? 500 : 0)
      .style('opacity', 1)
      .selection();

  vis.gLines = vis.gBorder.append('g')
      .attr('class', 'lines')
      .selectAll('line.border')
      .data(borderLines)
      .enter()
      .append('line')
      .attr('class', 'border')
      .attr('x1', d => { var xx = xCoord(d.source, vis); console.log(xx); return xx;})
      .attr('x2', d => xCoord(d.target, vis))
      .attr('y1', d => yCoord(d.source, vis))
      .attr('y2', d => yCoord(d.target, vis));

  vis.gVertices = vis.gBorder.append('g')
      .attr('class', 'vertices');

  vis.vertices = vis.gVertices.selectAll('g.vertex')
      .data(vertices)
      .enter()
      .append('g')
      .attr('class', 'vertex')
      .attr('transform', d =>'translate(' + xCoord(d, vis) + ',' + yCoord(d, vis) + ')');

  vis.vertices.append('text')
      .text(d => d.name)
      .attr('class', 'label')
      .classed('dashboard', !vis.config.hidden)
      .style('text-anchor', 'middle')
      .attr('x', d => {
        if (d.pct_spent_amazon === 1) {
          return -20
        } else if (d.pct_spent_walmart === 1) {
          return 0;
        } else {
          return 20;
        }
      })
      .attr('y', d => {
        if (d.pct_spent_walmart === 1) {
          return -5;
        } else {
          return 20;
        }
      });

  vis.wrangleData();
};
TriangleVis.prototype.wrangleData = function() {
  var vis = this;

  // Apply any filtering here
  vis.displayData = applyFilter(vis.data, vis.filter, ['pct_spent_amazon', 'pct_spent_walmart', 'pct_spent_other']);

  vis.updateVis();
};
TriangleVis.prototype.updateVis = function() {
  var vis = this;

  vis.scaleRadius.domain(d3.extent(vis.displayData, d => d.total_spent));

  vis.markers = vis.gMarkers.selectAll('circle.marker')
      .data(vis.displayData, d => d.key);


  vis.markers.enter()
        .append('circle')
        .attr('class', 'marker')
        .classed('dashboard', !vis.config.hidden)
      .merge(vis.markers)
      .transition(1000)
        .attr('cx', d => xCoord(d, vis))
        .attr('cy', d => yCoord(d, vis))
        .attr('r', d => vis.scaleRadius(d.total_spent))
      .selection()
      .on('mouseover', dbEventHandler.highlight)
      .on('mouseout', dbEventHandler.resetHighlight);

  vis.markers.exit()
      .transition(500)
      .style('opacity', 0)
      .on('end', function() { d3.select(this).remove(); });
};
TriangleVis.prototype.render = function() {
  var vis = this;
  vis.initVis();
};

function xCoord(d, vis) {
  var p = getCoords(d);

  var res = vis.x(.5 * (2 * p[1] + p[2]) / (p[0] + p[1] + p[2]));
  if (!res) {
    return vis.x(0);
  } else {
    return res;
  }
}
function yCoord(d, vis) {
  var p = getCoords(d);
  var res = vis.y(Math.sqrt(3) / 2 * (p[2] / (p[0] + p[1] + p[2])));
  if (!res) {
    return vis.y(0);
  } else {
    return res;
  }
}
function getCoords(d) {
  return [
    d.pct_spent_amazon,
    d.pct_spent_other,
    d.pct_spent_walmart
  ];
}

TriangleVis.prototype.applyFilter = function(filter) {
  var vis = this;

  vis.filter = filter;
  vis.wrangleData();
};
TriangleVis.prototype.highlight = function(key) {
  this.markers.classed('highlight', d => {
    return d.key === key;
  });
};
TriangleVis.prototype.resetHighlight = function() {
  this.markers.classed('highlight', false);
};
