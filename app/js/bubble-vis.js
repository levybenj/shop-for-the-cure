BubbleVis = function(_parentElement, _data, _config) {
  this.parentElement = _parentElement;
  this.data = _data;
  this.config = _config;

  this.initVis();
};

BubbleVis.prototype.initVis = function() {
  var vis = this;

  // Setup
  if (!vis.parentElement) {
    unpackConfig(vis);
    vis.gMarkers = vis.svg.select('g.markers');
  } else {
    vis.margin = {
      top: 60,
      bottom: 20,
      left: 60,
      right: 20
    };
    vis.width = $('#' + vis.parentElement).width() - vis.margin.left - vis.margin.right;
    vis.height = vis.width * .5;
    vis.svg = makeSvg(vis);
    vis.gMarkers = vis.svg.append('g').attr('class', 'markers');
  }

  // Groups
  // vis.gCircles = vis.svg.append('g').attr('class', 'circles');

  // Scales
  // For scaling total spent
  vis.scaleRadius = d3.scaleSqrt()
      .range([0, 10]);
  vis.radiusFixed = 2;

  var xx = 3,
      yy = 4;

  // For scaling CRM
  vis.x = d3.scaleOrdinal()
      .domain([0, 1])
      .range([vis.width / xx, vis.width * (xx - 1) / xx]);

  // For scaling SFTC
  vis.y = d3.scaleOrdinal()
      .domain([0, 13, 18, 50])
      .range([vis.height * (yy - 1) / yy, vis.height / yy, vis.height / yy, vis.height / yy]);

  // For also labelling CRM
  vis.scaleStroke = d3.scaleOrdinal()
      .domain([0, 1])
      .range(['var(--sa-gray)', 'var(--sa-blue)']);

  // Adding labels for groups
  vis.labels = [
    {text: 'P&G CRM', x: vis.x(1), y: -30},
    {text: 'Shopper Army', x: vis.x(0), y: -40},
    {text: 'Yes', x: 20, y: vis.y(50)},
    {text: 'No', x: 20, y: vis.y(0)},
    {text: 'Acquisition source', x: vis.width / 2, y: -60},
    {text: 'Participated in Shop for the Cure', x: -25, y: vis.height / 2 - 20, transform: 'rotate(-90)'}
  ];

  vis.gLabels = vis.svg.append('g')
      .attr('class', 'labels');

      // .attr('transform', d => d.transform || null);

  vis.wrangleData();
};
BubbleVis.prototype.wrangleData = function() {
  var vis = this;

  // Any filtering gets done here
  // FOR DEV: only using part of the data
  // vis.displayData = vis.data
  //     .filter((d, i) => i % 1 === 1);
  vis.displayData = vis.data;

  console.log(vis.displayData);
  vis.updateVis();
};
BubbleVis.prototype.updateVis = function() {
  var vis = this;

  console.log("Made it here!");

  vis.scaleRadius.domain(d3.extent(vis.displayData.map(d => d.total_spent)));

  var markers = vis.gMarkers.selectAll('circle.marker')
      .data(vis.displayData, d => d.key);

  vis.markers = markers.enter()
        .append('circle')
        .attr('class', 'marker')
      .attr('cx', 0).attr('cy', 0)
      .merge(markers)
        .attr('stroke', d => vis.scaleStroke(d.crm))
        .attr('fill', 'var(--sa-orange)')
      .transition(1000)
        .attr('cx', d => d.x)
        .attr('cy', d => d.y + 10)
        .attr('r', vis.radiusFixed);

  markers.exit().remove();
};

BubbleVis.prototype.render = function() {
  var vis = this;

  vis.gLabels.selectAll('text.label')
      .data(vis.labels)
      .enter()
      .append('text')
      .attr('class', 'label')
      .text(d => d.text)
      .attr('x', d => d.x)
      .attr('y', d => d.y)
      .style('text-anchor', 'middle')
      .call(wrap, 40)
      .style('opacity', 0)
      .transition(500)
      .style('opacity', 1);

  vis.updateVis();
};