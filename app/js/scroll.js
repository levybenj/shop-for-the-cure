var gs1,
    gs2;

var callbacks1 = [renderFlowVis, renderTimelineVis],
    callbacks2 = [journeyStart, journeyFocus, journeyAcquisition, journeyCarts,
                  journeyScatter, journeyTriangle, journeyEnd];

// First set of functions
function renderFlowVis() {
  flowVis.render();
}
function renderTimelineVis() {
  timelineVis.render();
}

// Second set of functions
function journeyStart() {
  console.log("Rendering bubble vis!");
  journeyVis.renderBubbleVis();
}
function journeyFocus() {
  console.log("Filtering data");
  journeyVis.filterData();
}
function journeyAcquisition() {
  console.log("Showing info");
  journeyVis.showInfo();
}
function journeyCarts() {
  console.log("Rendering block vis!");
  journeyVis.renderBlockVis();
}
function journeyScatter() {
  console.log("Rendering scatter vis");
  journeyVis.renderScatterVis();
}
function journeyTriangle() {
  console.log("Rendering triangle vis!");
  journeyVis.renderTriangleVis();
}
function journeyEnd() {
  console.log("Done journey");
}