/**
 * FlowVis -- Object constructor function
 * @param _parentElement
 * @param _data
 * @param _config
 * @param _eventHandler
 * @constructor
 */
FlowVis = function(_parentElement, _data, _config, _eventHandler) {
  this.parentElement = _parentElement;
  this.data = _data;
  this.displayData = _data;
  this.config = _config;
  this.eventHandler = _eventHandler;

  this.initVis();
};

FlowVis.prototype.initVis = function() {
  var vis = this;

  // Constants for display
  vis.nodeWidth = 15;
  vis.nodePadding = 25;

  vis.margin = {
    top: 40,
    bottom: 20,
    left: 70,
    right: 30
  };
  vis.width = $('#' + vis.parentElement).width() - vis.margin.left - vis.margin.right;
  vis.height = vis.width * .4;
  vis.svg = makeSvg(vis);

  vis.sankey = d3.sankey()
      .nodeId(d => d.name)
      .nodeWidth(vis.nodeWidth)
      .nodePadding(vis.nodePadding)
      .size([vis.width, vis.height])
      .nodeAlign(d3.sankeyCenter);

  vis.gLinks = vis.svg.append('g').attr('class', 'links');
  vis.gNodes = vis.svg.append('g').attr('class', 'nodes');

  // Level labels
  vis.gLevels = vis.svg.append('g').attr('class', 'levels')
      .selectAll('text.label')
      .data(['Acquisition source', 'SFTC purchase', 'Post-SFTC purchase'])
      .enter()
      .append('text')
      .attr('class', 'label')
      .attr('x', (d, i) => vis.width / 2 * i)
      .attr('y', -30)
      .text(d => d)
      .style('text-anchor', 'middle')
      .style('alignment-baseline', 'bottom')
      .call(wrap, 60);

  vis.wrangleData();
};
FlowVis.prototype.wrangleData = function() {
  var vis = this;

  // Nothing for now
  console.log(vis.displayData);
  vis.updateVis();
};
FlowVis.prototype.updateVis = function() {
  var vis = this;

  let graph = vis.sankey(vis.displayData);

  // Removing the NONE links/nodes
  graph.nodes = graph.nodes.filter(d => d.name !== 'post_purchase_none');
  graph.links = graph.links.filter(d => d.target.name !== 'post_purchase_none');

  let links = vis.gLinks.selectAll('path.link')
      .data(graph.links);
  let nodes = vis.gNodes.selectAll('g.node')
      .data(graph.nodes);

  links.enter()
      .append('path')
      .attr('class', 'link')
      .merge(links)
      .attr('d', d3.sankeyLinkHorizontal())
      .style('stroke-width', d => d.width)
      .on('mouseover', function(d) {
        vis.mouseover(this);
        linkLabs.style('opacity', e => e === d ? 1 : 0);
      })
      .on('mouseout', function() {
        vis.mouseout(this);
        linkLabs.style('opacity', 0);
      });

  // Link labels on mouseover
  let linkLabs = vis.gLinks.selectAll('text.label')
      .data(graph.links)
      .enter()
      .append('text')
      .attr('class', 'link')
      .text(d => d.value)
      .attr('x', d => (d.source.x1 + d.target.x0) / 2)
      .attr('y', d => (d.y0 + d.y1) / 2)
      .style('alignment-baseline', 'middle')
      .style('opacity', 0);

  var nodeEnter = nodes.enter()
      .append('g')
        .attr('class', 'node');

  nodeEnter.append('rect')
        .attr('class', 'node')
        .attr('x', 0)
        .attr('y', 0)
        .attr('width', d => d.x1 - d.x0)
        .attr('height', d => d.y1 - d.y0)
        .on('mouseover', function(d) {
          vis.mouseover(this);
          links.attr('class', e => d === e.source || d === e.target ? 'highlight link': 'link'); // This isn't working rn
          linkLabs.style('opacity', e => d === e.source || d === e.target ? 1 : 0);
          $(vis.eventHandler).trigger("levelChanged", d.depth);
        })
        .on('mouseout', function() {
          vis.mouseout(this);
          links.classed('highlight', false);
          linkLabs.style('opacity', 0);
        })
      .append('title')
        .text(d => d.name);

  nodeEnter.append('text')
      .attr('x', -6)
      .attr('y', d => (d.y1 - d.y0) / 2)
      .attr('dy', '.35em')
      .attr('class', 'label')
      .attr('text-anchor', 'end')
      .text(d => `${d.displayName}: ${d.value}`)
      .call(wrap, 25);

  nodeEnter.merge(nodes)
      .attr('transform', d => 'translate(' + d.x0 + ',' + d.y0 + ')')


};
FlowVis.prototype.mouseover = function(e) {
  d3.select(e).classed('highlight', true);
};
FlowVis.prototype.mouseout = function(e) {
  d3.select(e).classed('highlight', false);
};

FlowVis.prototype.render = function() {
  console.log("Rendering flow vis!");
};